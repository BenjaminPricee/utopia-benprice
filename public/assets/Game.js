const canvas = document.getElementById('the_canvas').getContext('2d');

window.onload = function () {
    document.addEventListener("keydown", Input);
    document.addEventListener("keyup", Input);
    setInterval(GameLoop, 30);
}

function GameLoop() {
	Update();
	Draw();
}

function KeyInput(input) {
    this.action = input;

}

// -------------------------------------------------------------------------------------------------------------


function Update() {
    console.log("A");

    // Player
    if (keyInput.action === "Up") {
        if (player.y > 0) {
            player.y -= PLAYER_SPEED;
        }
        frameY = 1;
        frameXDelay += 1;
    } else if (keyInput.action === "Down") {
        if (player.y < 625) {
            player.y += PLAYER_SPEED;
        }
        frameY = 0;
        frameXDelay += 1;
    } else if (keyInput.action === "Left") {
        if (player.x > 0) {
            player.x -= PLAYER_SPEED;
        }
        frameY = 2;
        frameXDelay += 1;
    } else if (keyInput.action === "Right") {
        if (player.x < 1125) {
            player.x += PLAYER_SPEED;
        }
        frameY = 3;
        frameXDelay += 1;
    } else if (keyInput.action === 'None') {
        frameX = 0;
    }

    if (frameXDelay > 10) {
        frameXDelay = 0;
        frameX++;
    }

    if (frameX >= 4) {
        frameX = 0;
    }
}

function Draw() {
	console.log("B");
    canvas.clearRect(0, 0, 1000, 1000);

    // Floor
    canvas.drawImage(floorImage,
        0,
        0);

    // Player
	canvas.drawImage(player.image,
		frameX * playerWidth,
		frameY * playerHeight,
		playerWidth,
		playerHeight,
		player.x,
		player.y,
		75,
        75);

    // Babies
    for (let x = 0; x < 3; x++) {
        for (let y = 0; y < 2; y++) {

            canvas.drawImage(babyImage,
                150 + (x * 400),
                y * 650,
                50,
                50);

        }
    }

    // Backdrop
    canvas.drawImage(backdropImage,
        0,
        0);

}

// -------------------------------------------------------------------------------------------------------------
let keyInput = new KeyInput("None");

// Key
function Input(evt) {
    if (event.type === "keydown") {
        console.log(event);
        switch (event.keyCode) {
            case 37:
                keyInput = new KeyInput("Left");
                break;
            case 38:
                keyInput = new KeyInput("Up");
                break;
            case 39:
                keyInput = new KeyInput("Right");
                break;
            case 40:
                keyInput = new KeyInput("Down");
                break;
            default:
                keyInput = new KeyInput("None");
        }
    } else {
        keyInput = new KeyInput("None");
    }
}

// Nipple
var dynamic = nipplejs.create({
    color: 'black',
    zone: document.getElementById('JoyStick'),
});
dynamic.on('added', function (evt, nipple) {
    nipple.on('dir:up', function (evt, data) {
        keyInput = new KeyInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        keyInput = new KeyInput("Down");
    });
    nipple.on('dir:left', function (evt, data) {
        keyInput = new KeyInput("Left");
    });
    nipple.on('dir:right', function (evt, data) {
        keyInput = new KeyInput("Right");
    });
    nipple.on('end', function (evt, data) {
        keyInput = new KeyInput("None");
    });
});

// -------------------------------------------------------------------------------------------------------------

function gameObject(image, x, y, width, height) {
	this.image = image;
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
}

// Player
let playerImage = new Image();
playerImage.src = "assets/images/Player.png";

let playerWidth = 133;
let playerHeight = 200
let frameX = 0;
let frameY = 0;
let frameXDelay = 0;
const PLAYER_SPEED = 3;
let player = new gameObject(playerImage, 0, 0, playerWidth, playerHeight);

// Babies
let babyImage = new Image();
babyImage.src = "assets/images/SFML-LOGO.png";

const BABY_COUNT = 6;

// Floor
let floorImage = new Image();
floorImage.src = "assets/images/Floor.png";

// Backdrop
let backdropImage = new Image();
backdropImage.src = "assets/images/Backdrop.png";