const canvas = document.getElementById('the_canvas').getContext('2d');

function gameObject(image, x, y, width, height) {
    this.image = image;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let image = new Image();
image.src = "assets/images/Player.png";

function KeyInput(input) {
    this.action = input;

}
// -------------------------------------------------------------------------------------------------------------

let Width = 133;
let Height = 200
let frameX = 0;
let frameY = 0;
let frameXDelay = 0;

let keyInput = new KeyInput("None");
let player = new gameObject(image, 0, 0, Width, Height);

// -------------------------------------------------------------------------------------------------------------

function inputHandler(event) {
    if (event.type === "keydown") {
        console.log(event);
        switch (event.keyCode) {
            case 37:
                keyInput = new KeyInput("Left");
                break;
            case 38:
                keyInput = new KeyInput("Up");
                break;
            case 39:
                keyInput = new KeyInput("Right");
                break;
            case 40:
                keyInput = new KeyInput("Down");
                break;
            default:
                keyInput = new KeyInput("None");
        }
    } else {
        keyInput = new KeyInput("None");
    }
}

// -------------------------------------------------------------------------------------------------------------

function update() {
    if (keyInput.action === "Up") {
        if (player.y > 0) {
            player.y -= 3;
        }
        frameY = 1;
        frameXDelay += 1;
    } else if (keyInput.action === "Down") {
        if (player.y < 700 - player.height) {
            player.y += 3;
        }
        frameY = 0;
        frameXDelay += 1;
    } else if (keyInput.action === "Left") {
        if (player.x > 0) {
            player.x -= 3;
        }
        frameY = 2;
        frameXDelay += 1;
    } else if (keyInput.action === "Right") {
        if (player.x < 1200 - player.width) {
            player.x += 3;
        }
        frameY = 3;
        frameXDelay += 1;
    } else if (keyInput.action === 'None') {
        frameX = 0;
    }

    if (frameXDelay > 10) {
        frameXDelay = 0;
        frameX++;
    }

    if (frameX >= 4) {
        frameX = 0;
    }
}

// -------------------------------------------------------------------------------------------------------------

function draw() {
    canvas.clearRect(0, 0, 1000, 1000);

    canvas.drawImage(player.image,
        frameX * Width,
        frameY * Height,
        Width,
        Height,
        player.x,
        player.y,
        player.width,
        player.height);
}

// -------------------------------------------------------------------------------------------------------------

function GameLoop() {
    window.requestAnimationFrame(GameLoop);
    update();
    draw();
}

// -------------------------------------------------------------------------------------------------------------

window.addEventListener('keydown', inputHandler);
window.addEventListener('keyup', inputHandler);

window.requestAnimationFrame(GameLoop);